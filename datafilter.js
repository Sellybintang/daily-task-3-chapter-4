// module.exports = {
//     filter : function(data, attribute,query){
//         let filtered_data = []
//         filtered_data = data.filter((data)=>{
//             return data.favoriteFruit === attribute && data.age >= query.age
//         })
//         return filtered_data
//     }
// }

module.exports = {
    filter : function(allData, attribute){
        if(attribute == undefined || attribute == ""){
            return allData
        }
        // let result = []
        let result = allData.filter((data)=> {
            return data.company.toLowerCase() === attribute;
        })
        return result
    }
}
